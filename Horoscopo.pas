program Horoscopo;

uses 
crt;

const
MaxUsers = 20;
S = 12;
PPS = 5;

type
user = record
nombre : string;
signo, predi : integer;
DDN, MDN : integer;
end;

var
usuario : array[1..Maxusers] of user;
cu : integer;
Mes : array[1..12] of String;
Signo : array[1..12] of string;
Pre : array[1..S,1..PPS] of string;
finished : integer;
procedure boot;
var
i : integer;
begin
for i:= 1 to maxusers do usuario[i].nombre := 'null';
for i:= 1 to maxusers do usuario[i].predi := 404;
finished := 0;
signo[1]:='Aries';
signo[2]:='Tauro';
signo[3]:='Geminis';
signo[4]:='Cancer';
signo[5]:='Leo';
signo[6]:='Virgo';
Signo[7]:='Libra';
signo[8]:='Escorpio';
signo[9]:='Sagitario';
signo[10]:='Capricornio';
signo[11]:='Acuario';
signo[12]:='Piscis'; 
Mes[1]:='Enero';
Mes[2]:='Febrero';
Mes[3]:='Marzo';
Mes[4]:='Abril';
Mes[5]:='Mayo';
Mes[6]:='Junio';
Mes[7]:='Julio';
Mes[8]:='Agosto';
Mes[9]:='Septiembre';
Mes[10]:='Octubre';
Mes[11]:='Noviembre';
Mes[12]:='Diciembre';                      
end;

function sig(ddn, mdn: integer):integer;
begin
case mdn of 
1 : if ddn<21 then sig:=10 else sig:=11;
2 : if ddn<19 then sig:=11 else sig:=12;
3 : if ddn<21 then sig:=12 else sig:=1;
4 : if ddn<21 then sig:=1 else sig:=2;
5 : if ddn<21 then sig:=2 else sig:=3;
6 : if ddn<22 then sig:=3 else sig:=4;
7 : if ddn<23 then sig:=4 else sig:=5;
8 : if ddn<23 then sig:=5 else sig:=6;
9 : if ddn<23 then sig:=6 else sig:=7;
10 : if ddn<23 then sig:=7 else sig:=8;
11 : if ddn<23 then sig:=8 else sig:=9;
12 : if ddn<22 then sig:=9 else sig:=10;
end;
end;

procedure registro;
var
i : Integer;
begin
clrscr;
Writeln('Introduce tu nombre!');
readln(usuario[cu].nombre);
Writeln('En cual mes naciste?');
Writeln('(Pon el numero correspondiente)');
for i:= 1 to 12 do writeln(i,'.',Mes[i]);
readln(usuario[cu].mdn);
writeln('En que dia?');
readln(usuario[cu].ddn);
usuario[cu].signo := sig(usuario[cu].ddn, usuario[cu].mdn);
writeln('Tu signo, es ',Signo[usuario[cu].signo]);
readln;
end;

procedure GenPre;
var
a, b: integer;
z, x, c, v : integer;
msj : array[1..4,1..4] of string;
begin
msj[1,1]:= 'Manana ';
msj[1,2]:= 'Hoy ';
msj[1,3]:= 'Mas Tarde ';
msj[1,4]:= 'Hoy ';
msj[2,1]:= 'Recibiras ';
msj[2,2]:= 'Te daran ';
msj[2,3]:= 'Perderas ';
msj[2,4]:= 'Descubriras ';
msj[3,1]:= 'Una Gran Bendicion, ';
msj[3,2]:= 'Una oportunidad, ';
msj[3,3]:= 'Algo inesperado, ';
msj[3,4]:= 'Algo de gran valor, ';
msj[4,1]:= 'Intenta explorar nuevos lugares.';
msj[4,2]:= 'Sal de tu zona de confort.';
msj[4,3]:= 'Intenta algo diferente.';
msj[4,4]:= 'Enfocate en lo que es importante.';
for a:=1 to S do for b:= 1 to PPS do pre[a, b]:= msj[1, random(3)+1]+ msj[2, random(3)+1]+ msj[3, random (3)+1]+ msj[4, random(3)+1];
end;


procedure lectura;
begin
if usuario[cu].predi = 404 then usuario[cu].predi:= random(4)+1;
writeln(usuario[cu].predi);
Writeln('Tu fortuna del dia de hoy es... ');
writeln(Pre[usuario[cu].signo, usuario[cu].predi]);
writeln;
readln;
end;

procedure salir;
begin
clrscr;
writeln('Estas Cerrando sesion');
writeln('Hasta pronto!');
Readln;
finished := 1;
end;

Procedure menu;
var
op : integer;
begin
clrscr;
if usuario[cu].nombre = 'null' then begin
writeln('Bienvenido, Que deseas hacer?');
writeln('1.Registro de usuario');
writeln;
end
else begin
writeln('Bienvenido ',usuario[cu].nombre,', Que deseas hacer?');
writeln('1.Modificar Datos');
writeln('2.Lectura de horoscopo');
writeln;
end;
writeln('9.Cerrar Sesion');
readln(op);
case op of 
1 : registro;
2 : if usuario[cu].nombre = 'null' then writeln('Opcion invalida, volviendo al menu')
else lectura;
9 : salir;
else begin
writeln('Opcion Invalida, volviendo al menú');
end;
end;
end;

procedure second_chance;
var
op : integer;
begin
Writeln('Sesion Finalizada');
writeln('Que deseas hacer?');
writeln('1. Iniciar una nueva sesion');
writeln;
Writeln('9. Salir del programa');
readln(op);
if cu<maxusers then if op = 1 then begin
cu := cu+1;
finished:= 0;
end
else
begin
writeln('Gracias por usar el programa');
readln;
end
else begin
writeln('Maximo de usuarios alcanzados por día. Cerrando programa');
readln;
end
end;

begin //Main
boot;
genpre;
cu:=1;
repeat
repeat
menu;
until finished = 1;
second_Chance
until finished = 1;
end.

