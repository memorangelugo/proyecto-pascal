program Fibonacci;

uses crt;

const
min = 1;
max = 46;
max2 = 1836311903;


var
menu1, menu2 : integer;
num1, tg, tg1: longint;

function input_num(mode:integer):longint;
var
numa : longint;
begin
readln(numa);
if numa<min then begin
writeln;
writeln('No puede ser un valor menor a ',min,'. Volviendo al menu');
writeln;
readln;
input_num:= -404;
end
else if numa>max2 then
begin
writeln('No puede ser un valor mayor a ',max2,'. Volviendo al menu');
writeln;
readln;
input_num:= -404;
end
else if mode=0 then input_num:=numa else
 if numa>max then
begin
writeln('No puede ser un valor mayor a ',max,'. Volviendo al menu');
writeln;
readln;
input_num:= -404;
end
 else input_num:=numa;
end;

function fibo(a : integer):longint;
var
F, F1, F2: longint;
t: integer;

begin
F1 := 0;
F2 := 1;
for t:=1 to a do begin
fibo:= f2;
F := F2;
F2 := F1 + F2;
F1 := F;
end;
end;

begin
repeat
clrscr;
writeln('Bienvenido al sistema');
writeln('Que deseas realizar?');
writeln;
writeln('1.Ver Informacion de la cadena Fibonacci');
writeln('2.Calcular si un numero pertenece a la cadena Fibonacci');
writeln('3.Calcular un elemento de la serie Fibonacci');
writeln;
writeln('9.Salir');
writeln;
readln(menu1);
case menu1 of
1: begin
clrscr;
writeln('Pagina (1/2)');
writeln;
writeln('En matematicas, la sucesion o serie de Fibonacci es la siguiente sucesion infinita de numeros naturales:');
writeln;
writeln('0, 1, 1, 2, 3, 5, 8, 13, 21, 34...');
writeln;
writeln('La sucesion comienza con los numeros 0 y 1 a partir de estos, cada termino es la suma de los dos anteriores');
writeln('es la de recurrencia que la define.');
writeln;
writeln('Esta sucesion fue descrita en Europa por Leonardo de Pisa, matematico italiano del siglo XIII');
writeln('tambien conocido como Fibonacci.');
writeln;
writeln('Tiene numerosas aplicaciones en ciencias de la computacion, matematica y teoria de juegos');
writeln;
writeln('Presiona ENTER para continuar.');
writeln;
readln;
clrscr;
writeln('Pagina (2/2)');
writeln('PASCAL y LA SERIE FIBONACCI');
writeln('Este programa, unicamente muestra la serie de fibonacci hasta su elemento 46, debido, a las limitaciones');
writeln('que este lenguaje posee');
writeln;
writeln('Para manejar valores numericos, podemos utilizar distintos tipos de variables.');
writeln('La mas comun, de tipo integer (que es solo para numero enteros)');
writeln('Solo puede contener numeros en un rango de -32768 hasta el 32767');
writeln('Por lo cual, hubiera limitado este programa hasta el elemento 23. (28657) ');
writeln;
writeln('En su lugar, se utilzaron variables longint!');
writeln('Que tienen un rango extendido desde -2147483648 hasta 2147483647');
writeln;
writeln('Permitiendo asi, poder calcular hasta el elemento 46, de la serie fibonnaci en Pascal');
writeln;
writeln('Presiona ENTER para volver al menu...');
readln;
end;
2:begin
clrscr;
Writeln('Ingrese el numero a Verificar. (Entre 1 y 1836311903)');
num1:= input_num(0);
tg1:= 1;
if num1<>-404 then begin
repeat
tg:= fibo(tg1);
tg1:= tg1+1;
until tg>= num1;
if tg=num1 then begin
writeln('El numero ',num1,' Si pertenece a la cadena fibonacci');
Writeln('Y es el elemento #',tg1-1,' de la cadena');
writeln;
writeln('Volviendo al menu...');
readln;
end
else
begin
writeln('El numero ',num1,' No pertenece a la cadena fibonacci');
writeln('Volviendo al menu');
readln;
end;
end;
end;

3:begin
clrscr;
writeln('Introduce el numero elemento de la serie que quieres vizualizar. (Entre 1 y 46)');
num1:= input_num(1);
if num1 <> -404 then begin
Writeln('Deseas vizualizar la serie completa hasta el elemento deseado?');
Writeln('1=Si');
Writeln('2=No');
readln(menu2);
for tg:=1 to num1 do begin
tg1:=fibo(tg);
if menu2=1 then begin
writeln(tg,'.',tg1);
delay(10);
end;
end;
writeln;
writeln('El elemento #',num1,' De la cadena fibonacci es : ',tg1);
writeln;
readln;
end;
end;

9: begin
clrscr;
writeln('Hasta luego!');
writeln('Espero hayas disfrutado el programa');
readln;
writeln('Realizado por : Rangel, Guillermo. Venezuela 2021');
readln;
end;


else begin
writeln;
writeln('Opcion invalida, Intenta de nuevo');
writeln;
readln;
end;
end;
until menu1=9;
end.

